%global srcname safe-netrc
%global modname safe_netrc
%global distname %{lua:name = string.gsub(rpm.expand("%{srcname}"), "[.-]", "_"); print(name)}

Name:          python-%{srcname}
Version:       1.0.1
Release:       1%{?dist}
Summary:       Safe netrc file parser
Packager:      Duncan Macleod <duncan.macleod@ligo.org>

License:       GPLv2+
URL:           https://pypi.org/project/%{srcname}/
Source0:       %pypi_source

Prefix:        %{_prefix}

BuildArch:     noarch

%if 0%{?rhel} == 0 || 0%{?rhel} >= 9
BuildRequires: pyproject-rpm-macros
%endif
BuildRequires: python3-devel
BuildRequires: python3dist(pip)
BuildRequires: python3dist(pytest)
BuildRequires: python3dist(setuptools)
BuildRequires: python3dist(wheel)

%global _description %{expand:
This package provides a subclass of the Python standard library netrc.netrc
class to add some custom behaviors.}

%description %_description

# -- python3-safe-netrc

%package -n python%{python3_pkgversion}-%{srcname}
Summary: %{summary}
%description -n python%{python3_pkgversion}-%{srcname} %_description

%files -n python%{python3_pkgversion}-%{srcname}
%doc README.md
%license LICENSE.md
%{python3_sitelib}/*

# -- build

%prep
%autosetup -n %{srcname}-%{version}
# hack together setup.{cfg,py} files to support old versions of setuptools
%if 0%{?rhel} > 0 || 0%{?rhel} < 9
cat > setup.cfg <<EOF
[metadata]
name = %{srcname}
version = %{version}
author-email = %{packager}
description = %{summary}
license = %{license}
license_files = LICENSE
url = %{url}
[options]
packages = find:
python_requires = >=3.6
install_requires =
EOF
cat > setup.py <<EOF
from setuptools import setup
setup()
EOF
%endif

%build
%if 0%{?rhel} == 0 || 0%{?rhel} >= 9
%pyproject_wheel
%else
%py3_build_wheel
%endif

%install
%if 0%{?rhel} == 0 || 0%{?rhel} >= 9
%pyproject_install
%else
%py3_install_wheel %{distname}-%{version}-*.whl
%endif

%check
PYTHONPATH=%{buildroot}%{python3_sitelib} %{__python3} -B -m pytest --pyargs %{modname}

# -- changelog

%changelog
* Mon Jun 03 2024 Duncan Macleod <duncan.macleod@ligo.org> 1.0.1-1
- Update to 1.0.0
- Use new python macros/requires

* Mon Jul 17 2023 Duncan Macleod <duncan.macleod@ligo.org> 1.0.0-3
- Update spec syntax, build own wheel

* Tue Feb 11 2020 Leo Singer <leo.singer@ligo.org> 1.0.0-2
- Don't override Python bytecode when running unit tests.

* Mon Feb 10 2020 Leo Singer <leo.singer@ligo.org> 1.0.0-1
- New upstream release
- Run unit tests

* Sun Feb 9 2020 Leo Singer <leo.singer@ligo.org> 0.0.1-2
- Use python3_pkgversion

* Fri Feb 7 2020 Leo Singer <leo.singer@ligo.org> 0.0.1-1
- Initial RPM release
